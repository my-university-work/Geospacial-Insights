import os
import re
import shutil

import numpy as np
import rasterio
from skimage import io
from skimage import measure
from rasterio.plot import show


"""
takes folder paths for images, building masks and solar masks. If the building mask
has no region props, that means there are no buidlings and therefore itself along with
the corresponding image and solar mask can be discarded, otherwise save them all for
later processing.
"""
def sort_images(images_dir, b_masks_dir, s_masks_dir):

    # check the image number and uses it to sort the list of file names
    regex = re.compile("[a-z]+([0-9]+).+[a-z]+")
    sorted_images = sorted(os.listdir(images_dir), key=lambda x: re.split(regex,x)[1])
    sorted_b_masks = sorted(os.listdir(b_masks_dir), key=lambda x: re.split(regex,x)[1])
    sorted_s_masks = sorted(os.listdir(s_masks_dir), key=lambda x: re.split(regex,x)[1])

    for i, (image, b_mask, s_mask) in enumerate(zip(sorted_images, sorted_b_masks, sorted_s_masks)):
        with rasterio.open(b_masks_dir+b_mask, dtype=np.float32) as raster_ds:
            b_mask_img = np.array(raster_ds.read())
        with rasterio.open(s_masks_dir + s_mask, dtype=np.float32) as raster_ds:
            s_mask_img = np.array(raster_ds.read())
        with rasterio.open(images_dir + image) as raster_ds:
            # image = raster_ds.read()
            # image = np.array(image)

            print(raster_ds.count)
            image = raster_ds.read()

            print(image.shape)
            print(image.dtype, np.max(image))
            # image = (image / np.max(image)).astype(np.uint8)
            image = image / image.max()






        b_labels = measure.label(b_mask_img, background=0)
        b_regions = measure.regionprops(b_labels)

        print(i)

        if len(b_regions):
            # io.imsave('/media/vanguard/GV_NTFS/initial_dataset/filtered/mul_images/image_'+str(i)+'.png', image)
            # shutil.move('/media/vanguard/GV_NTFS/initial_dataset/unfiltered/MUL-PanSharpen/'+image, '/media/vanguard/GV_NTFS/initial_dataset/filtered/mul_images/image_'+str(i)+'.tif')


            with rasterio.open(
                    '/media/vanguard/GV_NTFS/initial_dataset/filtered/mul_pan_images/image_' + str(i) + '.tif',
                    'w',
                    driver='GTiff',
                    height=image.shape[1],
                    width=image.shape[2],
                    count=8,
                    dtype=image.dtype,
                    # crs='+proj=latlong',
                    # transform=transform,
            ) as dst:
                dst.write(image)


            # copyfile(images_dir+image, '/media/vanguard/GV_NTFS/initial_dataset/filtered/mul_pan_imagesss/image_'+str(i)+'.tif')
            # io.imsave('/media/vanguard/GV_NTFS/initial_dataset/filtered/mul_imagess/image_' + str(i) + '.tif',
            #           image)

            # tifffile.imsave('/media/vanguard/GV_NTFS/initial_dataset/filtered/mul_images/image_'+str(i)+'.tif', image_arr)

            # io.imsave('/media/vanguard/GV_NTFS/initial_dataset/filtered/building_masks/building_mask_'+str(i)+'.png', b_mask_img)
            # io.imsave('/media/vanguard/GV_NTFS/initial_dataset/filtered/solar_masks/solar_mask_'+str(i)+'.png', s_mask_img)

mul_pan_images_dir = "/media/vanguard/GV_NTFS/initial_dataset/unfiltered/MUL-PanSharpen/"
rgb_images_dir = "/media/vanguard/GV_NTFS/initial_dataset/unfiltered/RGB-PanSharpen_images/"

s_mask_dir = "/media/vanguard/GV_NTFS/initial_dataset/unfiltered/solar_masks_pan/"
b_mask_dir = "/media/vanguard/GV_NTFS/initial_dataset/unfiltered/building_masks_pan/"

sort_images(mul_pan_images_dir, b_mask_dir, s_mask_dir)
