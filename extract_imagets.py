import csv
import os
import re
import shutil
import cv2
import numpy as np
import rasterio
from rasterio.plot import reshape_as_image, reshape_as_raster, show
from skimage import io
from skimage import measure
from scipy.misc import imsave
from rasterio.plot import show
from PIL import Image


# from rasterio.windows import Window

with open('png_imaget_building_labels.csv', mode="w") as csv_file:
    writer = csv.writer(csv_file)
    writer.writerow(["Image", "Solar Panels?"])


"""
This function takes a directory and returns
the sorted list based on the number in the files
"""
def get_sorted_folder(dir):
    # check the file number and uses it to sort the list of file names
    regex = re.compile("([0-9]+)")
    sorted_files = sorted(os.listdir(dir), key=lambda x: re.split(regex, x)[1])

    return sorted_files

"""
This function takes a path to an image and returns
that image
"""
def open_img(path):
    with rasterio.open(path, dtype=np.float32) as raster_ds:
        img = reshape_as_image(raster_ds.read())
        # img = img / img.max()
        #add 10 pix border to outside
        img = cv2.copyMakeBorder(img, 10, 10, 10, 10, cv2.BORDER_CONSTANT, value=[0, 0, 0])

    return img


"""
This function takes an array of building regions
and an array of solar panel regions for a particular
image. If a solar panel region is inside a building 
region it is added to an array.
Return value is an array with all the building regions
that contain a solar panel for an image
"""
def check_for_solar_panels(b_regions, s_regions):
    b_regions_with_solar_panels = []
    for b_region in b_regions:
        minr, minc, maxr, maxc = b_region.bbox

        for sm_region in s_regions:
            row, col = sm_region.centroid

            # checks if solar panel region center is in building region bbox
            # and if building region not already in array to filter duplicates
            # due to building having more than 1 solar panel
            if row >= minr and row <= maxr and col >= minc and col <= maxc and b_region not in b_regions_with_solar_panels:
                b_regions_with_solar_panels.append(b_region)

    return b_regions_with_solar_panels

# paths to masks and images directories
rgb_images_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/rgb_images/'
mul_pan_images_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/mul_pan_images/'



b_masks_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/building_masks/'
s_masks_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/solar_masks/'

# rgb_imaget_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/rgb_imagets_224/'
rgb_imaget_dir = '/media/vanguard/2TB/FYP/RGB/imaget'
sm_imaget_dir = '/media/vanguard/2TB/FYP/RGB/mask'

mul_pan_imaget_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/mul_pan_imagets_224/'


# train_pos_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/mul_pan_imaget_data_224_temp/train/pos/'
# train_neg_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/mul_pan_imaget_data_224_temp/train/neg/'
#
# #dir containing many negative imagets minus test imagets
# all_neg_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/imaget_data_224/training_set/negative/'
#
# test_pos_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/mul_pan_imaget_data_224_temp/test/pos/'
# test_neg_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/mul_pan_imaget_data_224_temp/test/neg/'

train_pos_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/rgb_imagets_224/train/pos/'
train_neg_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/rgb_imagets_224/train/neg/'

#dir containing many negative imagets minus test imagets
all_neg_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/imaget_data_224/training_set/negative/'

test_pos_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/rgb_imagets_224/test/pos/'
test_neg_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/rgb_imagets_224/test/neg/'



# the sorted images and masks
images = get_sorted_folder(rgb_images_dir)
b_masks = get_sorted_folder(b_masks_dir)
s_masks = get_sorted_folder(s_masks_dir)





def save(dir, file_name, imaget):
    # save as
    # imsave(mul_pan_imaget_dir + file_name, imaget)
    with rasterio.open(
            dir + file_name,
            'w',
            driver='GTiff',
            # driver='tiff',
            height=imaget.shape[1],
            width=imaget.shape[2],
            count=imaget.shape[0],
            # dtype=rasterio.uint16,
            dtype=imaget.dtype,
            # crs='+proj=latlong',
            # transform=transform,
    ) as dst:
        # dst.write(imaget.astype(rasterio.uint16))
        dst.write(imaget)




import matplotlib.pyplot as plt

#desired_size for imaget
desired_size = 224

image_current = 0

pos = 0
neg = 0
train_pos = 0
train_neg = 0
test_pos = 0
test_neg = 0

for i, (image, b_mask, s_mask) in enumerate(zip(images, b_masks, s_masks)):

    # path = str(rgb_imaget_dir)+str(image)

    # image = Image.open(mul_pan_images_dir+image)

    print(i+1)

    # print(path)

    # masks and image
    b_mask_img = open_img(b_masks_dir+b_mask)

    s_mask_img = open_img(s_masks_dir+s_mask)

    image = open_img(rgb_images_dir+image)

    # print("image shape: ", image.shape)

    # Apply index labels to region of pixels in the mask images.
    b_labels = measure.label(b_mask_img, background=0)
    s_labels = measure.label(s_mask_img, background=0)

    # Get the connected region object for each building component using its labels
    s_regions = measure.regionprops(s_labels)

    # filter those building regions with an area less that 1000
    b_regions = measure.regionprops(b_labels)
    b_regions = [b_region for b_region in b_regions if b_region.area >= 1000]

    # returns an array of all building regions in the mask that also
    # contain a solar panel region.
    b_regions_with_solar_panels = check_for_solar_panels(b_regions, s_regions)


    # plt.show(image)

    # Crops the buildings in the rgb image using the building mask regions
    # and saves is as a .png
    pad = 5
    for j, b_region in enumerate(b_regions):
        # bbox: min_row, min_col, max_row, max_col
        minr, minc, maxr, maxc = b_region.bbox

        #crop imaget w padding
        imaget = image[minr-pad:maxr+pad, minc-pad:maxc+pad]
        sm_imaget = s_mask_img[minr-pad:maxr+pad, minc-pad:maxc+pad]


        # [w, h]
        old_size = imaget.shape[:2]
        ratio = float(desired_size) / max(old_size)
        new_size = tuple([int(x * ratio) for x in old_size])

        imaget = cv2.resize(imaget, (new_size[1], new_size[0]))
        sm_imaget = cv2.resize(sm_imaget, (new_size[1], new_size[0]))


        #add black padding
        delta_w = desired_size - new_size[1]
        delta_h = desired_size - new_size[0]
        top, bottom = delta_h // 2, delta_h - (delta_h // 2)
        left, right = delta_w // 2, delta_w - (delta_w // 2)
        color = [0, 0, 0]

        imaget = cv2.copyMakeBorder(imaget, top, bottom, left, right, cv2.BORDER_CONSTANT,
                                    value=color)
        sm_imaget = cv2.copyMakeBorder(sm_imaget, top, bottom, left, right, cv2.BORDER_CONSTANT,
                                    value=color)


        # imaget = reshape_as_raster(imaget)
        # imaget = imaget / imaget.max()

        # print(imaget.shape)


        imaget_file_name = 'image_' + str(i) + '_imaget_' + str(j) + '.png'
        sm_imaget_file_name = 'mask_image_' + str(i) + '_imaget_' + str(j) + '.png'

        #add imaget to csv file name and indicate if it has a solar panel present
        with open('png_imaget_building_labels.csv', mode="a") as csv_file:
            writer = csv.writer(csv_file)

            #if building has a solar panel on, mark true and save is positive folder
            # else mark False and save in negative folder
            if b_region in b_regions_with_solar_panels:
                row = [imaget_file_name,sm_imaget_file_name]
            # else:
            #     row = [imaget_file_name, 0]

            #save row in csv file
            writer.writerow(row)


        if b_region in b_regions_with_solar_panels:


            # save(rgb_imaget_dir, file_name, imaget)
            imsave(rgb_imaget_dir + imaget_file_name, imaget)
            imsave(sm_imaget_dir + sm_imaget_file_name, sm_imaget)

            sm_imaget_dir


            # #if building has a solar panel on, mark true and save is positive folder
            # # else mark False and save in negative folder
            # if b_region in b_regions_with_solar_panels:
            #     row = [file_name, 1]
            #     pos += 1
            #     if pos <= 3070:
            #         train_pos += 1
            #         save(train_pos_dir, file_name, imaget)
            #
            #         # shutil.copy(imaget_dir + row[0], train_pos_dir)
            #     else:
            #         test_pos += 1
            #         save(test_pos_dir, file_name, imaget)
            #
            #         # shutil.copy(imaget_dir + row[0], test_pos_dir)
            # else:
            #     row = [file_name, 0]
            #     neg += 1
            #
            #     if neg % 5 == 0:
            #         if neg <= 3070:
            #             train_neg += 1
            #             save(train_neg_dir, file_name, imaget)
            #         elif neg > 3070 and neg <= 3570:
            #         # else:
            #             test_neg += 1
            #             save(test_neg_dir, file_name, imaget)
            #
            # #save row in csv file
            # writer.writerow(row)
            #
            # if pos == 3570 and neg == 3570:
            #     break






        #
        #
        #
        #
        #
        # # print(imaget.shape)
        #
        #

        # print(imaget.shape)
        #
        # imaget = imaget.reshape()
        # print(imaget.shape)



        # unnormalize, reshape
        # imaget = imaget / 2 + 0.5
        # imaget = imaget / imaget.max()

        # npimg = np.array(imaget)
        # np.transpose(npimg, (1, 2, 0))

        # print(npimg.shape)
        #
        # print(np.transpose(npimg, (1, 2, 0)).shape)
        # npimg = np.transpose(npimg, (1, 2, 0))
        # print(npimg.shape)
        # # show
        # plt.figure()
        # plt.imshow(npimg[:,:,[5,3,2]])
        # plt.axis('off')
        # # plt.title(labels.numpy())
        # plt.show()

        #
        # rasterio.plot.show(imaget[[3,2,1],:,:])
        # rasterio.plot.show(imaget[[4,2,1],:,:])
        # rasterio.plot.show(imaget[[5,2,1],:,:])
        # rasterio.plot.show(imaget[[6,2,1],:,:])
        # rasterio.plot.show(imaget[[7,2,1],:,:])




