Classification of properties using Machine Learning and Remote Sensing


Description:
Combining address databases and property boundary can allow council and
services to have a good understanding of the evolution of urban areas. Using
remote sensing and satellite images, it is possible to inspect a given property at
several dates. However, having operators looking at a large number of property can be very
expensive. Automated methods that would give basic information about a
property are therefore very desirable (e.g. are there solar panels on the roof, is
there a swimming pool, is there vegetation nearby). They can then be launched
on large area a constrain the human checks to a few interesting properties. This project will focus on predicting characteristics of properties using machine
learning classification approaches. Training and testing datasets will be made
available.