import csv

# imaget_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/rgb_imagets/'
# pos_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/imagets_data/positive/'
# neg_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/imagets_data/negative/'
#
# with open('building_labels.csv', mode="r") as csv_file:
#     has_header = csv.Sniffer().has_header(csv_file.read())
#     csv_file.seek(0)  # Rewind.
#     reader = csv.reader(csv_file)
#     if has_header:
#         next(reader)  # Skip header row.
#     pos = 0
#     neg = 0
#     line_count = 0
#     for row in reader:
#         line_count += 1
#         print(line_count)
#         with rasterio.open(imaget_dir + row[0], dtype=np.float32) as raster_ds:
#
#             raster = raster_ds.read()
#             img = reshape_as_image(raster)
#             img = img / img.max()
#
#             if pos != 3570:
#                 if row[1] == "True":
#                     pos += 1
#                     io.imsave(pos_dir + row[0], img)
#             if neg != 3570:
#                 if row[1] == "False":
#                     neg += 1
#                     io.imsave(neg_dir + row[0], img)
#             if pos == 3570 and neg == 3570:
#                 break
#     print("number of buildings with solar panels: "+str(pos))
#     print("number of buildings without solar panels: "+str(neg))
#

import shutil
import re

imaget_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/mul_pan_imagets_224/'
train_pos_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/mul_pan_imaget_data_224/train/pos/'
train_neg_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/mul_pan_imaget_data_224/train/neg/'

#dir containing many negative imagets minus test imagets
all_neg_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/imaget_data_224/training_set/negative/'

test_pos_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/mul_pan_imaget_data_224/test/pos/'
test_neg_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/mul_pan_imaget_data_224/test/neg/'

with open('imaget_data/building_labels.csv', mode="r") as csv_file:
    has_header = csv.Sniffer().has_header(csv_file.read())
    csv_file.seek(0)  # Rewind.
    reader = csv.reader(csv_file)
    if has_header:
        next(reader)  # Skip header row.

    # for _ in range(77350):  # skip the first 500 rows
    #     next(reader)

    pos = 0
    neg = 0
    train_pos = 0
    train_neg = 0
    test_pos = 0
    test_neg = 0
    line_count = 0
    for row in reader:
        line_count += 1
        print(line_count)

        name = re.split('(\.)', row[0])[0]
        new_name = name+'.tiff'


        if row[1] == "True":
            pos += 1
            if pos <= 3070:
                train_pos += 1
                shutil.copy(imaget_dir+new_name, train_pos_dir)
            else:
                test_pos += 1
                shutil.copy(imaget_dir+new_name, test_pos_dir)


        if line_count % 10 == 0:
            if row[1] == "False":
                neg += 1
                # shutil.copy(imaget_dir + new_name, all_neg_dir)
                if neg <= 3070:
                    train_neg += 1
                    shutil.copy(imaget_dir + new_name, train_neg_dir)
                elif neg > 3070 and neg <= 3570:
                # else:
                    test_neg += 1
                    shutil.copy(imaget_dir + new_name, test_neg_dir)


        if pos == 3570 and neg == 3570:
            break

    print("==================================================")
    print(f'Training set has {train_pos+train_neg} imagets, '
          f'{train_pos} with solar panels and '
          f'{train_neg} without solar panels')
    print("==================================================")
    print(f'Test set has {test_pos+test_neg} imagets, '
          f'{test_pos} with solar panels and '
          f'{test_neg} without solar panels')

