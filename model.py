import torch.nn as nn
import torch.nn.functional as F




class Net(nn.Module):

    # # create hidden layers and assign these layers to self
    # def __init__(self, num_classes):
    #     super(Net, self).__init__()
    #
    #     self.conv1 = nn.Conv2d(in_channels=3, out_channels=12, kernel_size=3, stride=1, padding=1)
    #     self.relu1 = nn.ReLU()
    #
    #     self.conv2 = nn.Conv2d(in_channels=12, out_channels=12, kernel_size=3, stride=1, padding=1)
    #     self.relu2 = nn.ReLU()
    #
    #     self.pool = nn.MaxPool2d(kernel_size=2)
    #
    #     self.conv3 = nn.Conv2d(in_channels=12, out_channels=24, kernel_size=3, stride=1, padding=1)
    #     self.relu3 = nn.ReLU()
    #
    #     self.conv4 = nn.Conv2d(in_channels=24, out_channels=24, kernel_size=3, stride=1, padding=1)
    #     self.relu4 = nn.ReLU()
    #
    #     self.fc = nn.Linear(in_features=24 * 128 * 128, out_features=num_classes)
    #
    # def forward(self, input):
    #     output = self.conv1(input)
    #     output = self.relu1(output)
    #
    #     output = self.conv2(output)
    #     output = self.relu2(output)
    #
    #     output = self.pool(output)
    #
    #     output = self.conv3(output)
    #     output = self.relu3(output)
    #
    #     output = self.conv4(output)
    #     output = self.relu4(output)
    #
    #     output = output.view(output.size(0), -1)
    #
    #     output = self.fc(output)
    #
    #     return output

    # create hidden layers and assign these layers to self
    def __init__(self, num_classes):
        super(Net, self).__init__()

        # output of layer 1 will be 128x128 imagets w 8 channels
        self.layer1 = nn.Sequential(
            nn.Conv2d(3, 8, 5),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))

        # output of layer 2 will be 64x64 imagets w 16 channels
        self.layer2 = nn.Sequential(
            nn.Conv2d(8, 16, 5),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))

        # # Dropout for regularization
        # self.dropout = nn.Dropout(p=0.5)

        # output of layer 3 will be 32x32 imagets w 32 channels
        self.layer3 = nn.Sequential(
            nn.Conv2d(16, 32, 3),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))

        # output of layer 4 will be 16x16 imagets w 64 channels
        self.layer4 = nn.Sequential(
            nn.Conv2d(32, 64, 3),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))

        #TODO: W14 due to padding??
        # fully connected layers
        # 1st layer size 64*8*8 nodes connected to 2nd layers of
        # self.fc1 = nn.Linear(64*29*29, 1000)
        self.fc1 = nn.Linear(64*13*13, 1000)
        self.fc2 = nn.Linear(1000, 100)
        self.fc3 = nn.Linear(100, num_classes)
        #
        # self.fc1 = nn.Linear(32*7*7, num_classes)

        # # Convolution 1
        # self.cnn1 = nn.Conv2d(in_channels=3, out_channels=16, kernel_size=5, stride=1, padding=2)
        # self.relu1 = nn.ReLU()
        #
        # # Max pool 1
        # self.maxpool1 = nn.MaxPool2d(kernel_size=2)
        #
        # # Convolution 2
        # self.cnn2 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=5, stride=1, padding=2)
        # self.relu2 = nn.ReLU()
        #
        # # Max pool 2
        # self.maxpool2 = nn.MaxPool2d(kernel_size=2)
        #
        # # Dropout for regularization
        # self.dropout = nn.Dropout(p=0.5)
        #
        # # Fully Connected 1
        # self.fc1 = nn.Linear(32 * 64 * 64, 2)


    def forward(self, x):

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = x.view(x.size(0), -1)

        x = self.fc1(x)
        x = self.fc2(x)
        x = self.fc3(x)

        return x

        # # Convolution 1
        # out = self.cnn1(x)
        # out = self.relu1(out)
        #
        # # Max pool 1
        # out = self.maxpool1(out)
        #
        # # Convolution 2
        # out = self.cnn2(out)
        #
        # out = self.relu2(out)
        #
        # # Max pool 2
        # out = self.maxpool2(out)
        #
        # # Resize
        # out = out.view(out.size(0), -1)
        #
        # # Dropout
        # out = self.dropout(out)
        #
        # # Fully connected 1
        # out = self.fc1(out)

        # return out



# # maintain metrics
# metrics = {
#     'accuracy': accuracy,
#     'top1': top1,
# }