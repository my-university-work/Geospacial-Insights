

import torch
import torchvision
import torchvision.datasets as datasets
import torchvision.transforms as transforms
import os
import time
import model as m
import matplotlib.pyplot as plt
import numpy as np

# Parameters
classes = ('negative', 'positive')
n_classes = len(classes)
batch_size = 20
cuda=False

test_dir = "imaget_data/test_set"

transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize(mean=[0.485, 0.456, 0.406],
                          std=[0.229, 0.224, 0.225])])

test_set = datasets.ImageFolder(root=os.path.join(test_dir), transform=transform)
#Dataloader is used to shuffle the data and segment it into batches, then load it in parrelel using workers
#Sample specified therefore shuffle must be false, and perfromed in the indices
test_loader = torch.utils.data.DataLoader(test_set, batch_size=batch_size,
                                          shuffle=True, num_workers=0)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
if torch.device == 'cuda':
    state = torch.load('checkpoint.pt')
else:
    state = torch.load('checkpoint.pt', map_location='cpu')

state_dict = state['state_dict']
model = m.Net(n_classes)
model.load_state_dict(state_dict)
# eval() used to set dropout and batch normalization layers to evaluation mode before running inference.
# This prevents inconsistent inference results.
model.eval()
model.to(device=device, dtype=torch.float)



dataiter = iter(test_loader)
images, labels = dataiter.next()


def imshow(img):
    # unnormalize, reshape
    img = img / 2 + 0.5
    npimg = img.cpu().numpy()
    npimg = np.transpose(npimg, (1, 2, 0))

    #show
    plt.figure()
    plt.imshow(npimg)
    plt.axis('off')
    plt.title(labels.cpu().numpy())
    plt.show()


# print images
imshow(torchvision.utils.make_grid(images))
print('GroundTruth: ', ' '.join('%5s' % classes[labels[j]] for j in range(10)))


images = images.to(device)
outputs = model(images)

_, predicted = torch.max(outputs, 1)

print('Predicted: ', ' '.join('%5s' % classes[predicted[j]]
                              for j in range(10)))

transform = transforms.Compose([
    transforms.ToTensor()
])

def predict_image(image):
    image_tensor = image.cpu().numpy()
    image_tensor = transform(image_tensor).float()

    image_tensor = image_tensor.unsqueeze_(0)
    # input = Variable(image_tensor)
    input = image_tensor.to(device)
    input = input.transpose(1, 2)
    output = model(input)
    index = output.data.cpu().numpy().argmax()
    return index


correct = 0
total = 0
with torch.no_grad():
    for data in test_loader:
        images, labels = data
        images, labels = images.to(device=device, dtype=torch.float), labels.to(device=device)

        outputs = model(images)
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum().item()

print(f'Accuracy of the network on the {len(test_set)} test images: %d %%' % (
    100 * correct / total))


class_correct = list(0. for i in range(n_classes))
class_total = list(0. for i in range(n_classes))
with torch.no_grad():
    for data in test_loader:
        images, labels = data
        images, labels = images.to(device=device, dtype=torch.float), labels.to(device=device)

        outputs = model(images)
        _, predicted = torch.max(outputs, 1)
        c = (predicted == labels).squeeze()
        for i in range(10):
            label = labels[i]
            class_correct[label] += c[i].item()
            class_total[label] += 1


for i in range(n_classes):
    print('Accuracy of %5s : %2d %%' % (
        classes[i], 100 * class_correct[i] / class_total[i]))


# fig=plt.figure(figsize=(10,10))
# for ii in range(len(images)):
#     image = images[ii]
#     index = predict_image(image)
#     sub = fig.add_subplot(1, len(images), ii+1)
#     res = int(labels[ii]) == index
#     sub.set_title(str(classes[index]) + ":" + str(res))
#     plt.axis('off')
#
#     # unnormalize, reshape
#     image = image / 2 + 0.5
#     npimg = image.cpu().numpy()
#     npimg = np.transpose(npimg, (1, 2, 0))
#
#     plt.imshow(npimg)
# plt.show()