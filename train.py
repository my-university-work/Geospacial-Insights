import torch
import torchvision
import torchvision.datasets as datasets
import torchvision.transforms as transforms
import os
import time

from opencv_torchvision_transforms.cvtorchvision import cvtransforms


# Parameters
#input and output band size
# input_size = 3
# output_size = 32
classes = ('negative', 'positive')
n_classes = len(classes)
n_epoch = 100
batch_size = 32
learning_rate = 0.001
cuda=False

# training_dir = "imaget_data/training_set"
# test_dir = "imaget_data/test_set"
#
# training_dir = "/media/vanguard/GV_NTFS/initial_dataset/filtered/mul_pan_imaget_data_224/train"
# test_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/mul_pan_imaget_data_224/test'


training_dir = "/media/vanguard/GV_NTFS/initial_dataset/filtered/rgb_imaget_data_224/training_set"
test_dir = '/media/vanguard/GV_NTFS/initial_dataset/filtered/rgb_imaget_data_224/training_set'


#.Compose allows for multiple manipulations
#COnvert dataset into a Pytorch Tensor and normalise the data so the NN trains better
#TODO: instead of a standard normalisation for each input channel (RGB), supply it with ((mean,), (std,)) of the datasets channels
# transform = transforms.Compose(
#     [transforms.ToTensor(),
#      transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])


transform = transforms.Compose(
    [cvtransforms.ToTensor(),
     # transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
     cvtransforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
     ]
)

# transform = cvtransforms.Compose(
#     [
#       cvtransforms.ToTensor(),
#       # cvtransforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
#       cvtransforms.Normalize((0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5), (0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5))
#
#     ]
# )

# trainset = torchvision.datasets.CIFAR10(root='./data', train=True,
#                                         download=True, transform=transform)
# trainloader = torch.utils.data.DataLoader(trainset, batch_size=4,
#                                           shuffle=True, num_workers=2)
#
# testset = torchvision.datasets.CIFAR10(root='./data', train=False,
#                                        download=True, transform=transform)
# testloader = torch.utils.data.DataLoader(testset, batch_size=4,
#                                          shuffle=False, num_workers=2)


# Datasets and loaders
#Dataset reads and transforms an image in a dataset.
train_set = datasets.ImageFolder(root=os.path.join(training_dir), transform=transform)
test_set = datasets.ImageFolder(root=os.path.join(test_dir), transform=transform)

#Dataloader is used to shuffle the data and segment it into batches, then load it in parrelel using workers
#Sample specified therefore shuffle must be false, and perfromed in the indices
train_loader = torch.utils.data.DataLoader(train_set, batch_size=batch_size,
                                           shuffle=True, num_workers=0)
test_loader = torch.utils.data.DataLoader(test_set, batch_size=batch_size,
                                          shuffle=True, num_workers=0)

import matplotlib.pyplot as plt
import numpy as np


'''
function to show an image batch for Dataloader
'''
def imshow(img, title=None):
    # unnormalize, reshape

    img = img.numpy().transpose((1, 2, 0))
    # mean = np.array([0.485, 0.456, 0.406])
    # std = np.array([0.229, 0.224, 0.225])
    mean = np.array([0.5, 0.5, 0.5])
    std = np.array([0.5, 0.5, 0.5])
    img = (std * img) + mean

    # img = img / img.max()
    # img = np.clip(img, 0, 1)

    plt.figure()
    plt.imshow(img)
    plt.axis('off')
    plt.title(labels.numpy())
    plt.show()


    # img = img / 2 + 0.5
    # # img = img / img.max()
    # npimg = img.numpy()



# get some images
images, labels = next(iter(train_loader))


print(f'images shape on batch size = {images.size()}')
print(f'labels shape on batch size = {labels.size()}')

# show images

#rgb
imshow(torchvision.utils.make_grid(images))
#rgb tiff
# imshow(torchvision.utils.make_grid(images[:,[4,2,1],:,:]))



# print labels for images
print(' '.join('%5s' % classes[labels[j]] for j in range(batch_size)))


#
# import sys
# sys.exit()

resnet = True
inception = False
vgg = False
custom = False

# if CUDA toolkit is present
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

if torch.cuda.device_count() >= 1:
    cuda = True
    print("Let's use", torch.cuda.device_count(), "GPUs!")
else:
    print("using cpu")



import torchvision.models as models
from collections import OrderedDict
import torch.nn as nn
import torch.nn.functional as F
import model as m

def setModel():
    if resnet:
        print("using resnet")
        model = models.resnet18(pretrained=False)

    if inception:
        print("using inception")
        model = models.inception_v3()

    if vgg:
        print("using vgg")
        model = models.vgg16()

    if custom:
        print("using Net")
        model = m.Net(n_classes)

    return model


model = setModel()

## print model layers
child_counter = 0
for child in model.children():
    print(" child", child_counter, "is -")
    print(child)
    child_counter += 1

# for child in model.children():
#     for param in child.parameters():
#         print("This is what a parameter looks like - \n",param)
#         break
#     break


# child_counter = 0
# for child in model.children():
#     if child_counter < 6:
#         print("child ",child_counter," was frozen")
#         for param in child.parameters():
#             param.requires_grad = False
#     elif child_counter == 6:
#         children_of_child_counter = 0
#         for children_of_child in child.children():
#             if children_of_child_counter < 1:
#                 for param in children_of_child.parameters():
#                     param.requires_grad = False
#                 print('child ', children_of_child_counter, 'of child',child_counter,' was frozen')
#             else:
#                 print('child ', children_of_child_counter, 'of child',child_counter,' was not frozen')
#             children_of_child_counter += 1

#     else:
#         print("child ",child_counter," was not frozen")
#     child_counter += 1

# child_counter = 0
# for child in model.children():


#     if child.children():
#       for children_of_child in child.children():
#         print("child ", children_of_child)
# #           if children_of_child_counter < 1:
# #               for param in children_of_child.parameters():
# #                   param.requires_grad = False
# #               print('child ', children_of_child_counter, 'of child',child_counter,' was frozen')
# #           else:
# #               print('child ', children_of_child_counter, 'of child',child_counter,' was not frozen')
# #           children_of_child_counter += 1


#     if child_counter < 6:
#         print("child ",child_counter," was frozen")
#         for param in child.parameters():
#             param.requires_grad = False
#     elif child_counter == 6:
#         children_of_child_counter = 0
#         for children_of_child in child.children():
#             if children_of_child_counter < 1:
#                 for param in children_of_child.parameters():
#                     param.requires_grad = False
#                 print('child ', children_of_child_counter, 'of child',child_counter,' was frozen')
#             else:
#                 print('child ', children_of_child_counter, 'of child',child_counter,' was not frozen')
#             children_of_child_counter += 1

#     else:
#         print("child ",child_counter," was not frozen")
#     child_counter += 1


# ## Freezing all layers
# for params in model.parameters():
#     params.requires_grad = False

## Freezing the first few layers. Freezing the first 4 layers
# ct = 0
# for name, child in model.named_children():
#     ct += 1
#     if ct < 4:
#         for name2, params in child.named_parameters():
#           params.requires_grad = False
#     if ct == 4:
#       children_of_child_counter = 0
#       for children_of_child in child.children():
#         if children_of_child_counter < 1:
#             for param in children_of_child.parameters():
#                 param.requires_grad = False


## Change the last layer to fit problem
num_ftrs = model.fc.in_features
model.fc = nn.Linear(num_ftrs, n_classes)

if torch.cuda.is_available():
    model.cuda()
# cast model to CPU or GPU if available
model.to(device=device, dtype=torch.float)









import torch.optim as optim

criterion = nn.CrossEntropyLoss()

# if(checkpoint_epoch <= 50):
#   learning_rate = 0.1
# elif(checkpoint_epoch > 50 or checkpoint_epoch <= 80):
#     learning_rate = 0.01
# elif(checkpoint_epoch > 80 or checkpoint_epoch <= 100):
#     learning_rate = 0.001


optimizer = optim.Adam(model.parameters(), lr=learning_rate)
# optimizer = optim.SGD(model.parameters(), lr = learning_rate, momentum=0.9)
# optimizer = optim.SGD(filter(lambda p: p.requires_grad, model.parameters()), lr = learning_rate, momentum=0.9)

# Update non frozen params
# optimizer = optim.Adam(filter(lambda p: p.requires_grad, model.parameters()), lr=learning_rate)





def accuracy(output, target, topk=(1,)):
    """Computes the precision@k for the specified values of k"""
    maxk = max(topk)
    # batch_size = target.size(0)

    _, pred = output.topk(maxk, 1, True, True)
    pred = pred.t()
    correct = pred.eq(target.view(1, -1).expand_as(pred))

    res = []
    for k in topk:
        correct_k = correct[:k].view(-1).float().sum(0)
        res.append(correct_k.mul_(100.0 / batch_size))
    return res


hold_loss = []





def save_checkpoint(epoch, model, optimizer, train_losses, test_losses, val_accuracies, is_best=False,
                    filename='checkpoint.pt'):
    """saves the values of the network
           Args:
               epoch: (int) The current epoch the model was training on
               model: (torch.nn.Module) the neural network
               optimizer: (torch.optim) optimizer for parameters of model
               epoch_losses: (array) loss values for the model for each epoch

       """
    state = {
        'epoch': epoch + 1, 'state_dict': model.state_dict(),
        'optimizer': optimizer.state_dict(), 'train_loss': train_losses,
        'test_loss': test_losses, 'val_accuracies': val_accuracies,
    }
    # saves the models paramters
    # flexibility for restoring the model later, after major refactoring
    if not os.path.exists('checkpoint.pt'):
        os.mknod('checkpoint.pt')

    torch.save(state, filename)

    if is_best:
        save_checkpoint(epoch, model, optimizer, is_best=True, filename='model_best.pt')





def load_checkpoint():
    state = torch.load('checkpoint.pt')
    global checkpoint_epoch
    checkpoint_epoch = state['epoch']

    global train_losses
    train_losses = state['train_loss']

    global test_losses
    test_losses = state['test_loss']

    global val_accuracies
    val_accuracies = state['val_accuracies']

    global optimizer
    optimizer.load_state_dict(state['optimizer'])

    state_dict = state['state_dict']
    global model

    setModel()

    model.load_state_dict(state_dict)
    # eval() used to set dropout and batch normalization layers to evaluation mode before running inference.
    # This prevents inconsistent inference results.
    model.eval()

    model.cuda()

    model.to(device=device, dtype=torch.float)


train_losses, test_losses, val_accuracies = [], [], []

checkpoint_epoch = 0
if os.path.isfile("checkpoint.pt"):
    load_checkpoint()





import visualizations
from matplotlib.ticker import MaxNLocator
from sklearn import metrics


def test():
    print('=' * 30)
    print('TESTING')
    print('=' * 30)
    print()

    model.eval()

    test_loss = 0

    #     class_correct = list(0. for i in range(n_classes))
    #     class_total = list(0. for i in range(n_classes))

    total = 0
    correct = 0
    accuracy = 0

    with torch.no_grad():
        for i, (inputs, labels) in enumerate(test_loader):

            inputs, labels = inputs.cuda(), labels.cuda()

            if cuda:
                inputs, labels = inputs.to(device=device, dtype=torch.float), labels.to(device=device)

            outputs = model.forward(inputs)
            #           print(outputs)
            _, predicted = torch.max(outputs, 1)
            #           print(predicted)

            c = (predicted == labels).squeeze()

            loss = criterion(outputs, labels)
            test_loss += outputs.shape[0] * loss.item()

            ps = torch.exp(outputs)
            equality = (labels.data == ps.max(dim=1)[1])
            accuracy += outputs.shape[0] * equality.type(torch.FloatTensor).mean()

            batch_correct = (predicted == labels).sum().item()

            total += labels.size(0)
            correct += (predicted == labels).sum().item()


            # print statistics
            if (i + 1) % 5 == 0:  # print every 10 mini batches

                print('Step [{}/{}], Loss: {:.4f}, Accuracy: {:.2f}%'
                      .format(i + 1, len(test_loader), loss.item(),
                              (batch_correct / labels.size(0)) * 100))

                print("tn, fp\nfn, tp")
                print(
                    "Confusion matrix:\n%s" % metrics.confusion_matrix(labels.view(-1).cpu(), predicted.view(-1).cpu()))

    print('Accuracy: {:.2f}%'
          .format((correct / total) * 100))

    return accuracy, test_loss






for epoch in range(checkpoint_epoch, n_epoch):  # loop over the dataset multiple times

    print('=' * 30)
    print('TRAINING')
    print('=' * 30)
    print()
    print('Epoch {}/{}'.format(epoch + 1, n_epoch))
    print('=' * 30)

    """Train for one epoch on the training set
            Args:
                model: (torch.nn.Module) the neural network
                optimizer: (torch.optim) optimizer for parameters of model
                criterion: (torch.nn) xxxxxxxxx
                loss_fn: a function that takes batch_output and batch_labels and computes the loss for the batch
                metrics: (dict) a dictionary of functions that compute a metric using the output and labels of each batch
        """

    # set to training mode
    model.train()

    running_loss = 0.0
    train_loss = 0
    val_loss = 0
    val_acc = 0

    for i, (inputs, labels) in enumerate(train_loader):

        # data_time.update(time.time() - end)

        # get the images and labels of batch
        # inputs, labels = data

        # zero the parameter gradients
        optimizer.zero_grad()
        inputs, labels = inputs.cuda(), labels.cuda()

        # if cuda, move your data to the GPU
        if cuda:
            inputs, labels = inputs.to(device=device, dtype=torch.float), labels.to(device=device)

        # compute output, forward pass
        outputs = model(inputs)
        # comp diff, calc loss
        loss = criterion(outputs, labels)
        # compute gradients
        loss.backward()
        # update weights between nodes using optimiser
        optimizer.step()

        # loss is averaged by batch_size, which is the first dimension of outputs
        train_loss += outputs.shape[0] * loss.item()
        #         print('train set length: %d' % (len(train_set)))
        #         print('train loader length: %d' % (len(train_loader)))
        #         print('output shape: %d' % (outputs.shape[0]))
        #         print('output loss: %.3f' % (loss))
        #         print('training loss: %.3f' % (train_loss))

        # Total number of labels
        total = labels.size(0)

        # Obtaining predictions from max value
        _, predicted = torch.max(outputs, 1)

        #           print(outputs)
        #         print("outputs:", predicted)

        c = (predicted == labels).squeeze()

        #         print(c)

        # Calculate the number of correct answers
        correct = (predicted == labels).sum().item()

        # print statistics
        if (i + 1) % 10 == 0:  # print every 10 mini batches

            print('Epoch [{}/{}], Step [{}/{}], Loss: {:.4f}, Accuracy: {:.2f}%'
                  .format(epoch + 1, n_epoch, i + 1, len(train_loader), loss.item(),
                          (correct / total) * 100))

            print("tn, fp\nfn, tp")
            print("Confusion matrix:\n%s" % metrics.confusion_matrix(labels.view(-1).cpu(), predicted.view(-1).cpu()))

        #             test_acc, test_loss = test()

        # test_loss = 0
        # accuracy = 0
        # model.eval()
        # with torch.no_grad():
        #     for inputs, labels in test_loader:
        #         if cuda:
        #             inputs, labels = inputs.to(device=device, dtype=torch.float), labels.to(device=device)
        #
        #         logps = model.forward(inputs)
        #         batch_loss = criterion(logps, labels)
        #         test_loss += batch_loss.item()
        #
        #         ps = torch.exp(logps)
        #         top_p, top_class = ps.topk(1, dim=1)
        #         equals = top_class == labels.view(*top_class.shape)
        #         accuracy += torch.mean(equals.type(torch.FloatTensor)).item()

        # print(f"Train loss: {train_loss/len(train_loader)}.. "
        #       f"Test loss: {test_loss/len(test_loader):.3f}.. "
        #       f"Test accuracy: {accuracy/len(test_loader):.3f}")

        # train_loss = running_loss / len(train_loader)

        # val_acc = accuracy
        # val_loss = test_loss

        # train_losses.append(train_loss)

        #             print('[%d, %5d] loss: %.3f' %
        #                   (epoch + 1, i + 1, train_loss / len(train_loader)))

        running_loss = 0.0

    train_loss = train_loss / len(train_set)
    train_losses.append(train_loss)

    # Evaluate on the test set
    test_acc, test_loss = test()

    test_loss /= len(test_set)
    test_losses.append(test_loss)
    print('test losses', test_loss)

    test_acc /= len(test_set)
    val_accuracies.append(test_acc)
    print('val accuracies', test_acc)

    print(f"Train loss: {train_loss}.. "
          f"Test loss: {test_loss:.3f}.. "
          f"Test accuracy: {test_acc:.3f}")

    # test_loss = test_loss / len(test_loader)
    # return train_loss, test_loss

    # train_loss, test_loss = train_epoch(model, optimizer, criterion)

    # show
    plt.figure()
    plt.plot(train_losses, label='Training loss')
    plt.plot(test_losses, label='Validation loss')
    plt.plot(val_accuracies, label='Validation Acc')
    plt.ylabel("loss")
    plt.xlabel("n epoch")
    plt.title("loss rate")
    plt.legend()
    plt.ticklabel_format(style='plain', axis='x', useOffset=False)
    plt.show()

    save_checkpoint(epoch, model, optimizer, train_losses, test_losses, val_accuracies, filename="checkpoint.pt")
    # load_checkpoint()

print('Finished Training')














dataiter = iter(test_loader)
images, labels = dataiter.next()

# print images
imshow(torchvision.utils.make_grid(images))
print('GroundTruth: ', ' '.join('%5s' % classes[labels[j]] for j in range(1)))


images = images.to(device)
outputs = model(images)

_, predicted = torch.max(outputs, 1)

print('Predicted: ', ' '.join('%5s' % classes[predicted[j]]
                              for j in range(1)))

def predict_image(image):
    image_tensor = transforms.ToTensor(image).float()
    image_tensor = image_tensor.unsqueeze_(0)
    # input = Variable(image_tensor)
    input = image_tensor.to(device)
    output = model(input)
    index = output.data.cpu().numpy().argmax()
    return index



correct = 0
total = 0
with torch.no_grad():
    for data in test_loader:
        images, labels = data
        images, labels = images.to(device=device, dtype=torch.float), labels.to(device=device)

        outputs = model(images)
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum().item()

print(f'Accuracy of the network on the {len(test_set)} test images: %d %%' % (
    100 * correct / total))


class_correct = list(0. for i in range(n_classes))
class_total = list(0. for i in range(n_classes))
with torch.no_grad():
    for data in test_loader:
        images, labels = data
        images, labels = images.to(device=device, dtype=torch.float), labels.to(device=device)

        outputs = model(images)
        _, predicted = torch.max(outputs, 1)
        c = (predicted == labels).squeeze()
        for i in range(10):
            label = labels[i]
            class_correct[label] += c[i].item()
            class_total[label] += 1


for i in range(n_classes):
    print('Accuracy of %5s : %2d %%' % (
        classes[i], 100 * class_correct[i] / class_total[i]))



# def accuracy(out, labels):
#   outputs = np.argmax(out, axis=1)
#   return np.sum(outputs==labels)/float(labels.size)

