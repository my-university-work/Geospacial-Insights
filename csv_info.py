import csv

with open('imaget_data/building_labels.csv', mode="r") as csv_file:
    has_header = csv.Sniffer().has_header(csv_file.read())
    csv_file.seek(0)  # Rewind.
    reader = csv.reader(csv_file)
    if has_header:
        next(reader)  # Skip header row.
    solar_panel_imagets = 0
    line_count = 0
    for row in reader:
        line_count += 1
        if row[1] == "True":
            solar_panel_imagets += 1

    print("number of buildings with solar panels: "+str(solar_panel_imagets))
    print("total number of buildings " + str(line_count))
    print("{0:.1f}% of buildings have solar panels.".format((solar_panel_imagets/line_count)*100))
